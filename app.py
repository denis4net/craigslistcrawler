#!/usr/bin/env python3
import os
import binascii
import time
import sys
import telegram
import logging
import threading
import json
import pickle
from craigslist import CraigslistForSale
from pymongo import MongoClient

class Config(object):
    def __init__(self):
        self.mongo_url = os.environ.get("MONGODB_URL", None)
        self.telegram_token = os.environ.get("TELEGRAM_TOKEN", None)
        self.telegram_webhook = os.environ.get("TELEGRAM_WEBHOOK_HOST", None)

config = Config()
client = MongoClient(config.mongo_url)
db = client.craigslistcrawler
bot = telegram.Bot(token=config.telegram_token)

def notify(subscriber, res):
    bot.send_message(chat_id=subscriber['telegram_chat_id'], text="{url} {price} {where}".format(**res))

def poll(subscriber):
    subscriber['filter'].update({'posted_today': True})

    cl_h = CraigslistForSale(site='sfbay', area='sfc', category='cta', filters=subscriber['filter'])
    t = subscriber["latest"]

    for res in cl_h.get_results(sort_by='newest', geotagged=True):
        ad_t = time.mktime(time.strptime(res["datetime"], "%Y-%m-%d %H:%M"))
        t = max(ad_t, t) 
        if (ad_t <= subscriber["latest"]):
            break
    
        logging.debug("%d: %s", ad_t, res)
        notify(subscriber, res)

    db.subscribers.update_one(
        {"_id": subscriber["_id"]},
        {"$set": {'latest': int(t)}}
    )

def crawler_events_loop():
    for subscriber in db.subscribers.find():
        logging.debug("%s", subscriber)
        poll(subscriber)
    
if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    crawler_events_loop()