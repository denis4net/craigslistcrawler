FROM python:3.7
RUN pip3 install pipenv

# -- Install Application into container:
RUN set -ex && mkdir /app
WORKDIR /app
COPY app.py bot.py /app/

COPY Pipfile Pipfile
COPY Pipfile.lock Pipfile.lock
RUN pipenv install --deploy --system
